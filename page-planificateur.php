<?php
/*
Template Name: Planificateur
*/
get_header(); ?>


	<?php /* Start loop */ ?>
	<?php while (have_posts()) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			
			
			<div class="row">
				<div class="large-12 columns">
					<?php the_content(); ?>
				</div>
			</div>
				
			
			
		</article>
	<?php endwhile; // End the loop ?>


<?php get_footer(); ?>