function Midway(){
	var jQuerycenterHorizontal = jQuery('.midway-horizontal'),
		jQuerycenterVertical = jQuery('.midway-vertical');

	jQuerycenterHorizontal.each(function(){
		jQuery(this).css('marginLeft', -jQuery(this).outerWidth()/2);
	});
	jQuerycenterVertical.each(function(){
		jQuery(this).css('marginTop', -jQuery(this).outerHeight()/2);
	});
	jQuerycenterHorizontal.css({
		'display' : 'inline',
		'position' : 'absolute',
		'left' : '50%'
	});
	jQuerycenterVertical.css({
		'display' : 'inline',
		'position' : 'absolute',
		'top' : '50%',
	});
}
jQuery(window).on('load', Midway);
jQuery(window).on('resize', Midway);
