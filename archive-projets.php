
<?php
/*
Template Name: portfolio
*/
get_header(); ?>
<div id="portfolio">

<div class="slider">
  <h1 class="title">Dernier projet</h1>
  <div class="content-side">

   <span class="icon"><i class="fa fa-ge"></i></span>

</div>
  <?php if ( function_exists( 'soliloquy' ) ) { soliloquy( 'featured-work', 'slug' ); } ?>

  <!-- share -->
  <div class="share">
    <?php echo do_shortcode( '[easy-share buttons="facebook,twitter,more,google,buffer,linkedin" counters=1 counter_pos="inside" native="no" total_counter_pos="leftbig"] ') ?>
  </div>
  <!-- End share -->

</div>

<h2 class="title">Projets</h2>
<h1 class="title">Portfolio</h1>



<div class="content-side">

   <span class="icon"><i class="fa fa-briefcase"></i></span>

</div>

<div id="tagline" class="large-6 columns large-centered">
     <p>Voici une petite selection de nos réalisations</p>
</div>

<ul id="filter" class="large-6 columns large-centered">

<li class="filter" data-filter="all">Projets</li>
 <?php
$categories =  get_terms( 'projet-cat');
foreach ( $categories as $category )
     echo '<li class="filter" data-filter="'.$category->slug.'"><span>'.$category->slug.'</span></li>';
?>

</ul>

<ul id="grid" class="small-block-grid-1 large-block-grid-4 ">

     <?php


     $my_query = new WP_Query( 'post_type=projets&categories=websites&posts_per_page=-1' );
     while ( $my_query->have_posts() ) : $my_query->the_post(); ?>

     <li class="mix <?php if ( function_exists( 'Projet_categorie' ) ) {Projet_categorie(); }?>">

          <?php
          $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'night-thumb-portfolio' );
          $url = $thumb['0']; ?>

           <a href="<?php the_permalink(); ?>">
                      <div class="thumb">
                      <img class="lazy" data-original="<?php echo $url; ?>" data-largesrc="<?php echo $url; ?>" >

                      </div>

                <h4><?php the_title(); ?></h4>

                <div class="category">
                <?php the_terms( $post->ID, 'projet-cat' ); ?>
                </div>

          </a>
           </li>

     <?php endwhile;  wp_reset_query(); ?>
 </ul>

     </div>

<?php get_footer(); ?> 