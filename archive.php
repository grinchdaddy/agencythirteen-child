<?php get_header(); ?>
<div id="archive" class="row">
<!-- Row for main content area -->
	<div class="small-12 large-12 columns" id="content" role="main">
	<div class="large-8 columns">
		<h2 class="title">	<?php single_cat_title(); ?></h2>
		
		<ul class="archive-list ">		
	<?php if ( have_posts() ) : ?>
	
		<?php /* Start the Loop */ ?>
		<?php while ( have_posts() ) : the_post(); ?>

			
			<?php get_template_part( 'content-category', get_post_format() ); ?>
		<?php endwhile; ?>
		
		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		
	<?php endif; // end have_posts() check ?>
	</ul>
	<?php /* Display navigation to next/previous pages when applicable */ ?>
	<?php if ( function_exists('reverie_pagination') ) { reverie_pagination(); } else if ( is_paged() ) { ?>
		<nav id="post-nav">
			<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'reverie' ) ); ?></div>
			<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'reverie' ) ); ?></div>
		</nav>
	<?php } ?>

	</div>
	<div class="side large-4 columns">
		 <?php // Dynamic Sidebar
	if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'widget-top' ) ) : ?>

	
	<?php endif; // End Dynamic Sidebar store ?>

	 <?php // Dynamic Sidebar
	if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'shop' ) ) : ?>

	
	<?php endif; // End Dynamic Sidebar store ?>
</div>	
</div>
	
		</div>
<?php get_footer(); ?>