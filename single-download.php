<?php get_header(); ?>

	<div id="main-content" class="container">
		
		<div class="row">
		
				<?php if (have_posts()) : ?>
		
					<?php while (have_posts()) : the_post(); ?>
						
						<div id="page-product" class="large-12 columns">
							
							<div class="content large-8 columns">							
							
								<div class="entry product-content">
				
				
									<?php the_post_thumbnail('product-image-large'); ?>
									<ul class="small-block-grid-2 medium-block-grid-3 large-block-grid-4">
										<?php 
											if( function_exists( 'edd_di_display_images') ) {
											    edd_di_display_images();
											}
										 ?>
									</ul>
									<?php the_content('Read the rest of this entry &raquo;'); ?>

									<!-- Share button -->
								<div class="product-share">
									<h4><?php _e('Share our products', 'agency') ?></h4>
									<?php echo do_shortcode( '[easy-share buttons="facebook,twitter,google,pinterest,linkedin,buffer" counters=1 message="no" native="selected" show_fblike="yes" total_counter_pos="leftbig"] ') ?>
								
								</div><!-- End Share button -->

								</div><!--end .product-content.entry-->
							
							</div><!--end .content-->
						<div class="large-4 columns">
							<div class="product-meta content panel widget">
								
								<h2 class="title"><?php the_title(); ?></h2>
								
								<?php if(!edd_has_variable_prices($post->ID)) { ?>
									<h4 class="single-product-price"><?php edd_price($post->ID); ?></h4>
								<?php } ?>		
											
								<?php echo edd_get_purchase_link($post->ID, 'Add to Cart', 'button', 'blue'); ?>
								
								<div class="product-categories">
									<?php the_terms( $post->ID, 'download_category', '<span class="product-categories-title">Categories:</span> ', ', ', '' ); ?>
								</div><!--end product-categories-->
								
							
								
								<div class="product-tags">
									<?php the_terms( $post->ID, 'download_tag', '<span class="product-tags-title">Tags:</span> ', ', ', '' ); ?>
								</div><!--end .product-tags-->
								

									<!-- number sales -->
									<?php if (function_exists('nightmare_edd_show_download_sales')) { echo nightmare_edd_show_download_sales( ); } ?>
									<!-- end number sales -->

									<div class="sales-product">
									<?php echo do_shortcode( '[easy-social-like facebook=”true” facebook_width="100" twitter_tweet="true" google="true" pinterest_pin="true" counters="true" align="true"] ') ?>
									</div>
								
									<?php // Dynamic Sidebar
									if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'download-infos' ) ) : ?>
									
										<!-- Sidebar fallback content -->
									
									<?php endif; // End Dynamic Sidebar download-infos ?>
								 
								<!-- End number sales -->

								 <?php // Dynamic Sidebar
							if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'shop' ) ) : ?>
							
								<!-- Sidebar fallback content -->
							
							<?php endif; // End Dynamic Sidebar shop  ?>

								

							</div><!--end .product-meta-->
							
							
							
						</div><!--end .fourcol-->		
						</div><!--end .eightcol-->
						
												
						
					<?php endwhile; ?>
		
					<div class="navigation">
						<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
						<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
					</div><!--end .navigation-->
		
				<?php else : ?>
		
					<div class="entry product-content not-found">
	
						<h2>Not Found</h2>
						<p>Sorry, but you are looking for something that isn't here.</p>
						<?php get_search_form(); ?>
						
					</div><!--end .product-content.entry-->
				
				
				<?php endif; ?>
			
		</div><!--end .row-->

	</div><!--end #main-content.container-->

<?php get_footer(); ?>