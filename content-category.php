<?php
/**
* The default template for displaying content. Used for both single and index/archive/search.
*
* @subpackage Nightmare
* @since Nightmare 2.0
*/
?>
<li class="archive small-12 columns">
    <div class="thumb small-12 large-4 columns">
        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'thumb-blog'); ?></a>
    </div>
    
<div class="small-12 large-8 columns">

<h2 class="entry-title">
<a href="<?php the_permalink(); ?>"><?php the_title( ); ?></a>
</h2>
<div class="author-content">
        <?php nightmare_meta() ?>
        <span class="time">
        <?php
                $days = round((date('U') - get_the_time('U')) / (60*60*24));
                if ($days==0) {
                echo "Publié aujourd'hui";
                }
                elseif ($days==1) {
                echo "Publié hier";
                }
                else {
                echo '<span class="fa fa-clock-o"></span> ' . $days . " jours";
                }
        ?>
        </span>
    </div>

<?php the_excerpt(); ?>

</div>
</li>
<li class="separation"></li>