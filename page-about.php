<?php
/*
Template Name: About
*/
get_header(); ?>
	

<div class="row">


	<?php /* Start loop */ ?>
	<?php while (have_posts()) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
			
		</article>
	<?php endwhile; // End the loop ?>

<div id="partenaires" class="section">
	<h2>Nos partenaires</h2>

	<hr />
	<?php
	$args = array(
		'post_type' =>  'partenaire',
		'posts_per_page'  => 4,
	 );
	$query_name = new WP_Query( $args );
	?>
	<ul class="partenaire-container">
	<?php if ( $query_name->have_posts() ) : ?>
		<?php while ( $query_name->have_posts() ) : $query_name->the_post(); ?>
	
			<?php
			
				$key = '_link';
				$wdn_meta = get_post_meta($post->ID, $key, true);
			
				// if the field is set within wordpress, continue
				if (get_post_meta($post->ID, $key, true))  ?>
			
				  	<li class="partenaires">
					<a href="<?php echo $wdn_meta  ?>"><?php the_post_thumbnail( ); ?></a> 
					</li>
				
			
	
	<?php endwhile;endif; wp_reset_query(); ?>

</ul>	

<!-- Devenir partenaire -->
<hr>
<div class="action-partenaire">
	<p>Vous désirez devenir un nos partenaires ?</p>
	<a href="/contact" target="_blank" class="button radius tiny">Devenir notre partenaire</a>
	<a href="/nos-partenaires" target="_blank" class="button radius tiny">Liste de nos partenaires</a>
</div>
<!-- End Devenir partenaire -->

</div>
<div id="tweets" class="section">
	<h2 class="text-center">Nos tweets</h2>

	<hr />

	<span class="bird"> <i class="fa fa-twitter"></i>
	</span>
	<div class="view">
		
		 <?php echo do_shortcode( " [rotatingtweets screen_name='Agencythirteen' include_rts='1' tweet_count='1' timeout='3000']") ?>
	</div>
</div>
</div>



<?php get_footer(); ?>