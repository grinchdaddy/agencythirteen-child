<?php
/**
 * Nightmare Theme Functions
 * @version 2.0
 * @since 0.1.0
 * @package Nightmare
 * @author Greg Petithomme <hello@agencythirteen.net>
 * @copyright Copyright (c) 2014, Greg Petithomme
 */

//Partenaires
add_action( 'after_setup_theme', 'partnaires' );
function partnaires() {
	if ( ! class_exists( 'Super_Custom_Post_Type' ) )
		return;

	$partenaires = new Super_Custom_Post_Type( 'partenaire', 'Partenaire', 'Partenaires' );
	$partenaires->set_icon( 'user' );
	$partenaires->add_meta_box( array(
		'id' => 'link',
		'context' => 'advanced',
		'fields' => array(
			'_link' => array('type' => 'text'),
		)
	) );
}

?>