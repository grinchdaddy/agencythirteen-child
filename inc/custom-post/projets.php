<?php 
add_action( 'after_setup_theme', 'projets' );
function projets() {
	if ( ! class_exists( 'Super_Custom_Post_Type' ) )
		return;

	$projets = new Super_Custom_Post_Type( 'projets', 'Projet', 'Projets' );
	$projets->set_icon( 'briefcase' );
	$projet_cats = new Super_Custom_Taxonomy( 'projet-cat', 'Projet Cat', 'Projet Cats' );
	connect_types_and_taxes( $projets, $projet_cats );

$projets->add_meta_box( array(
	'id' => 'informations',
	'context' => 'side',
	'fields' => array(
		'Clients' => array('type' =>'text'),
		'Skill'   => array('type' =>'text'),
		'Site'    => array('type' =>'text'),
	)
) );

$projets->add_meta_box( array(
	'id' => 'projet-thumbnail',
	'context' => 'advanced',
	'fields' => array(
		'thumbnails' => array('type' => 'media'),
	)
) );
$projets->add_to_columns( '_thumbnail_id' );
}

function Projet_categorie(){
     global $post;
 
    //get terms for CPT
    $terms = get_the_terms( $post->ID , 'projet-cat' ); 
                //iterate through array
                foreach ( $terms as $termprojet ) { 
                    //echo taxonomy name as class
                    echo ' '.$termprojet->name; 
                } 
}
 ?>
