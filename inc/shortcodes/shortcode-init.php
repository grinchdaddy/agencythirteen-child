<?php 
  function shortcode_empty_paragraph_fix( $content ) {

        $array = array (
            '<p>[' => '[',
            ']</p>' => ']',
            ']<br />' => ']'
        );

        $content = strtr( $content, $array );

        return $content;
    }

    add_filter( 'the_content', 'shortcode_empty_paragraph_fix' );

/*==========  Row Shortcode  ==========*/

function row_shortcode( $atts , $content = null ) {

	// Attributes
	extract( shortcode_atts(
		array(
			'row' => 'row',
		), $atts )
	);
	return '<div class="'.$row.'">'.do_shortcode($content).'</div>';


}

add_shortcode( 'row', 'row_shortcode' );


/*==========  Grid Shortcode  ==========*/

function grid_shortcode( $atts , $content = null ) {

	// Attributes
	extract( shortcode_atts(
		array(
			'columns' => '12',
		), $atts )
	);
	return '<div class="large-'.$columns.' columns">'.do_shortcode($content).'</div>';
}
add_shortcode( 'grid', 'grid_shortcode' );


/*==========  Button Shortcode  ==========*/

function button_shortcode( $atts , $content = null ) {

	// Attributes
	extract( shortcode_atts(
		array(
			'size'      => '',
			'target'    => '',
			'statut'    => '',
			'radius'	=> '',
			'color'		=> '',
		), $atts )
	);
	return '<a href="'.$target.'" class="button '.$size.' '.$statut.' '.$radius.' '.$color.' ">'.do_shortcode($content).'</a>';
}
add_shortcode( 'button', 'button_shortcode' );


/*==========  Label Shortcode  ==========*/

function label_shortcode($atts , $content = null  ) {
	extract( shortcode_atts( 
		array(
		'color' => '',
		'radius' => '',

	), $atts ) 
	);

	return '<span class="'.$color.' '.$radius.' label">'.do_shortcode($content).'</span>';
}
add_shortcode( 'label', 'label_shortcode' );
 ?>