<?php 
//Thumbnails 
add_image_size( 'shop-large', 705, 600, true );
add_image_size( 'shop-small', 150, 113, true );

//EDD Coming Soon
function edd_coming_soon_ag( $custom_text ) {

    return '<h2>' . $custom_text . '</h2>';

}
add_filter( 'edd_coming_soon_display_text', 'edd_coming_soon_ag' );


function agn_edd_di_display_images( $html, $download_image ) {
    // here a div tag is wrapped around each image
	$html = '<li class="edd-di-image"><a href="' . $download_image['image'] . '" rel="lightbox"><img src="' . $download_image['image'] . '" /></a></li>';
    return $html;
}
add_filter( 'edd_di_display_images', 'agn_edd_di_display_images', 10, 2 );
 ?>