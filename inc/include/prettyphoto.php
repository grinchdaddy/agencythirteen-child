<?php 
if (!is_admin()) {
	add_action("wp_enqueue_scripts", "register_scripts", 11);
}
function register_scripts() {
	// wp_deregister_script('jquery');
	// wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js", false, null);
	// wp_enqueue_script('jquery');
	
	wp_enqueue_script( 'prettyPhoto_script', get_stylesheet_directory_uri()."/components/jquery-prettyPhoto/js/jquery.prettyPhoto.js", '', '3.1.5', false );

	wp_enqueue_style( 'prettyPhotto_style', get_stylesheet_directory_uri()."/components/jquery-prettyPhoto/css/prettyPhoto.css", '','3.1.5', 'screen' );
};
 ?>