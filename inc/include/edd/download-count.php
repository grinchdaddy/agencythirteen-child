<?php
 
/**
 * Show the number of sales and download count inside the "Download Details" widget
 */
function nightmare_edd_show_download_sales() {
	echo '<div class="sales-product">';
	echo '<i class="fa fa-shopping-cart"></i> ';
	echo  edd_get_download_sales_stats( get_the_ID() );
	echo ' ';
	echo _e(' sales','agency');
	echo '</div>';
	// echo '<div class="sales-product">';
	// echo nightmare_edd_get_download_count( get_the_ID() ) . ' downloads';
	// echo '</div>';
}
add_action( 'edd_product_details_widget_before_purchase_button', 'nightmare_edd_show_download_sales' ); 
 
/**
 * Get the download count of a download
 * Modified version of edd_get_file_downloaded_count()
 */
function nightmare_edd_get_download_count( $download_id = 0 ) {
	global $edd_logs;
 
	$meta_query = array(
		'relation'	=> 'AND',
		array(
			'key' 	=> '_edd_log_file_id'
		),
		array(
			'key' 	=> '_edd_log_payment_id'
		)
	);
 
	return $edd_logs->get_log_count( $download_id, 'file_download', $meta_query );
}

?>