<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @subpackage Nightmare
 * @since Nightmare 2.0
 */
?>

<li class="small-12 columns">
<div class="thumb">
	<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'thumb-blog'); ?></a>
	
	<div class="category">
		 <?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?>
	</div>
	<div class="author-content">
	<?php nightmare_meta() ?>

<span class="time">
	<?php 
	$days = round((date('U') - get_the_time('U')) / (60*60*24));
	if ($days==0) {
		echo "Publié aujourd'hui"; 
	}
	elseif ($days==1) {
		echo "Publié hier"; 
	}
	else {
		echo '<span class="fa fa-clock-o"></span> ' . $days . " jours";
	} 
?>
</span>
</div>

</div>
<h2 class="entry-title">
	<a href="<?php the_permalink(); ?>"><?php the_title( ); ?></a>

</h2>
<?php the_excerpt(); ?>

<hr>
</li>

