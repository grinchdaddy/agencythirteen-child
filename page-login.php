<?php
/*
Template Name:login
 */
get_header('shop'); ?>
	<div id="content"  class="row">
		

	<div id="login" role="main">
	<h2 class="text-center"><?php _e('Sign In', 'agency') ?></h2>
	<?php
if ( ! is_user_logged_in() ) { // Display WordPress login form:
    $args = array(
        'echo'           => true,
        'redirect'       => site_url( $_SERVER['REQUEST_URI'] ), 
        'form_id'        => 'login-form',
        'label_username' => __( 'Username' ),
        'label_password' => __( 'Password' ),
        'label_remember' => __( 'Remember Me' ),
        'label_log_in'   => __( 'Log In' ),
        'id_username'    => 'user_login',
        'id_password'    => 'user_pass',
        'id_remember'    => 'rememberme',
        'id_submit'      => 'wp-submit',
        'remember'       => true,
        'value_username' => NULL,
        'value_remember' => true
    );
    wp_login_form( $args );
} else { // If logged in:
    wp_loginout( home_url() ); // Display "Log Out" link.
    echo " | ";
    wp_register('', ''); // Display "Site Admin" link.
}
?>

	</div>
</div>
		
<?php get_footer(); ?>