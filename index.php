<?php get_header(); ?>

<div class="row">
<!-- Row for main content area -->
	<div class="small-12 large-8 columns" id="content" role="main">
	<ul  id="blog-page" class="post">
	<?php if ( have_posts() ) : ?>
		
	<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'content-blog', get_post_format() ); ?>
		<?php endwhile; ?>
		
		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		
	<?php endif; // end have_posts() check ?>

	</ul>
	<?php /* Display navigation to next/previous pages when applicable */ ?>
	<?php if ( function_exists('nightmare_pagination') ) { nightmare_pagination(); } else if ( is_paged() ) { ?>
		<nav id="post-nav">
			<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'agency' ) ); ?></div>
			<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'agency' ) ); ?></div>
		</nav>
	<?php } ?>

	</div>
	<div class=" small-12 large-4 columns bio">
	<?php global $options; ?>
		<div class="panel widget">
		<div id="bio-face" style="background :  url('<?php  echo $options ['background-bio'] ['url'];?>'); background-size:cover ">
		
		<img src="<?php echo $options ['face-bio'] ['url']; ?>"  class="face" alt="">
		</div>
		<h3><?php global $options; echo $options ['name'] ?></h3>
		<p> <?php global $options; echo $options ['bio'] ?></p>
	</div>
	<div class="side">
		<?php // Dynamic Sidebar
		if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'widget-top' ) ) : ?>
		
		<?php endif; // End Dynamic Sidebar blog ?>

		<?php // Dynamic Sidebar
		if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'blog' ) ) : ?>
		
		<?php endif; // End Dynamic Sidebar blog ?>
	</div>
	</div>
	
		</div>
<?php get_footer(); ?>