<?php 
        register_nav_menus(array(
            'primary' => __('Primary Navigation', 'nightmare'),
            'top-bar-l' => __('Nav Left', 'nightmare'),
            'top-bar-r' => __('Nav Right', 'nightmare'),
            'right-off' => __('Canvas Off', 'nightmare'),

        ));
 
 /**
 *
 * Nav normal
 *
 **/
function nightmare_top_bar() {
    wp_nav_menu(array( 
        'container' => false,                           // remove nav container
        'container_class' => '',                        // class of container
        'menu' => '',                                   // menu name
        'menu_class' => 'top-bar-menu ',            // adding custom nav class
        'theme_location' => 'primary',                // where it's located in the theme
        'before' => '',                                 // before each link <a> 
        'after' => '',                                  // after each link </a>
        'link_before' => '',                            // before each link text
        'link_after' => '',                             // after each link text
        'depth' => 5,                                   // limit the depth of the nav
        'fallback_cb' => false,                         // fallback function (see below)
        'walker' => new nightmare_walker()
    ));
}


 /**
 *
 * Nav left
 *
 **/
function nightmare_top_bar_l() {
    wp_nav_menu(array( 
        'container' => false,                           // remove nav container
        'container_class' => '',                        // class of container
        'menu' => '',                                   // menu name
        'menu_class' => 'top-bar-menu left',            // adding custom nav class
        'theme_location' => 'top-bar-l',                // where it's located in the theme
        'before' => '',                                 // before each link <a> 
        'after' => '',                                  // after each link </a>
        'link_before' => '',                            // before each link text
        'link_after' => '',                             // after each link text
        'depth' => 5,                                   // limit the depth of the nav
        'fallback_cb' => false,                         // fallback function (see below)
        'walker' => new nightmare_walker()
    ));
}

 /**
 *
 * Nav right
 *
 **/
function nightmare_top_bar_r() {
    wp_nav_menu(array( 
        'container' => false,                           // remove nav container
        'container_class' => '',                        // class of container
        'menu' => '',                                   // menu name
        'menu_class' => 'top-bar-menu right',            // adding custom nav class
        'theme_location' => 'top-bar-r',                // where it's located in the theme
        'before' => '',                                 // before each link <a> 
        'after' => '',                                  // after each link </a>
        'link_before' => '',                            // before each link text
        'link_after' => '',                             // after each link text
        'depth' => 5,                                   // limit the depth of the nav
        'fallback_cb' => false,                         // fallback function (see below)
        'walker' => new nightmare_walker()
    ));
}

 /**
 *
 * Nav right
 *
 **/
function nightmare_right_off() {
    wp_nav_menu(array( 
        'container' => false,                           // remove nav container
        'container_class' => '',                        // class of container
        'menu' => '',                                   // menu name
        'menu_class' => 'off-canvas-list',            // adding custom nav class
        'theme_location' => 'right-off',                // where it's located in the theme
        'before' => '',                                 // before each link <a> 
        'after' => '',                                  // after-er each link </a>
        'link_before' => '',                            // before each link text
        'link_after' => '',                             // after each link text
        'depth' => 5,                                   // limit the depth of the nav
        'fallback_cb' => false,                         // fallback function (see below)
        'walker' => new nightmare_walker()
    ));
}

 ?>