<?php
/**********************
Enqueue CSS and Scripts
**********************/
add_action('after_setup_theme','nightmare_startup');
// loading modernizr and jquery, and reply script
if( ! function_exists( 'nightmare_scripts_and_styles ' ) ) {
	function nightmare_scripts_and_styles() {
	  if (!is_admin()) {

  	    wp_register_script( 'night-script', get_stylesheet_directory_uri() . '/js/script.js', array(), '1.0.0', true );

	    // modernizr (without media query polyfill)
	    wp_register_script( 'nightmare-modernizr', get_template_directory_uri() . '/js/modernizr.min.js', array(), '2.6.2', false );

	    // register Google font
	    wp_register_style('google-font', 'http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|Lora:400,700|Droid+Sans+Mono');

	    // ie-only style sheet
	    wp_register_style( 'nightmare-ie-only', get_template_directory_uri() . '/css/ie.css', array(), '' );

	    // comment reply script for threaded comments
	    if( get_option( 'thread_comments' ) )  { wp_enqueue_script( 'comment-reply' ); }
	    
	    // adding Foundation scripts file in the footer
	    wp_register_script( 'nightmare-js', get_stylesheet_directory_uri() . '/js/foundation.min.js', array( 'jquery' ), '', true );

	    // FontAwesome
	   	wp_register_style( 'nightmare-fontawesome', '//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css', array(), '' );

        wp_register_script( 'night-icheck', get_stylesheet_directory_uri() . 'js/vendor/bower_components/iCheck/icheck.min.js', array(), '1.0.1', true );


      wp_enqueue_script( 'scrollreveal',  get_stylesheet_directory_uri() . '/bower_components/scrollReveal.js/dist/scrollReveal.min.js', array( 'jquery' ), '', true );

     //  //iCheck
    	// wp_enqueue_script( 'night-icheck',  get_stylesheet_directory_uri() . '/bower_components/iCheck/icheck.js', array( 'jquery' ), '', true );

       wp_enqueue_script( 'nightmare-js' );
      


	  }
	}
}



if( ! function_exists( 'nightmare_homepage ' ) ) {
	function nightmare_homepage(){
		if ( is_front_page() ) {
		
	    wp_register_script( 'nightmare-midway', get_template_directory_uri() . '/bower_components/Midway.js/midway.js', array(), '1.2', false );
	    wp_enqueue_script( 'nightmare-midway' );

		}

	}
}
add_action( 'wp_print_scripts', 'nightmare_homepage'); 





?>