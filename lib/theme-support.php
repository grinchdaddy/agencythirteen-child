<?php
function agency_theme_support() {
    // Add language support
    load_theme_textdomain('agency', get_stylesheet_directory() . '/lang');

    // Add menu support
    add_theme_support('menus');

    // Add post thumbnail support: http://codex.wordpress.org/Post_Thumbnails
    add_theme_support('post-thumbnails');
    // set_post_thumbnail_size(150, 150, false);

    add_image_size( 'night-thumb-portfolio', 440, 310, array('center', 'top') );
    add_image_size( 'night-full-portfolio', 770, 552,true );
    add_image_size( 'night-blog', 760, 350, true );
    add_image_size( 'night-full', 600, 350, true );
    add_image_size( 'night-blog-full', 1600, 550, true );
    add_image_size( 'night-network', 620, 413, array('center', 'top') );

    // rss thingy
    add_theme_support('automatic-feed-links');

    // Add post formarts support: http://codex.wordpress.org/Post_Formats
    add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));

}

add_action('after_setup_theme', 'agency_theme_support'); 
?>