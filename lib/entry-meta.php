<?php 

if ( ! function_exists( 'nightmare_meta' ) ) {
    function nightmare_meta() {
        echo '<span class="byline author">'. __('Written by ', 'agency') . get_the_author() .' </span>';
       
    }
};

 ?>