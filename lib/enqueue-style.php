<?php
/*********************
Enqueue the proper CSS
if you use Sass.
*********************/
if( ! function_exists( 'nightmare_enqueue_style' ) ) {
	function nightmare_enqueue_style()
	{
		// foundation stylesheet
		wp_register_style( 'nightmare-agency', get_stylesheet_directory_uri() . '/css/agency.css', array(), '' );
		wp_register_style('nightmare-fontawesome', '//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css' );
		$upload_dir = wp_upload_dir();
		wp_register_style( 'ngf-settings',  $upload_dir['baseurl'] .'/ngf-settings.css');

		wp_enqueue_style( 'nightmare-fontawesome' );
		wp_enqueue_style( 'nightmare-agency' );
		wp_enqueue_style( 'ngf-settings' );
		
	}
}
add_action( 'wp_enqueue_scripts', 'nightmare_enqueue_style' );

?>