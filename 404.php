<?php get_header(); ?>

<!-- Row for main content area -->
	<div id="error" role="main">
	<div class="small-12 large-8 large-centered column">
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1 class="entry-title"><?php _e('File Not Found', 'nightmare'); ?></h1>
			</header>
			<div class="entry-content">
				<div class="error">
				<span class="logo-error">
					<img src="<?php echo get_stylesheet_directory_uri() ?>/img/logo/logo.png" class="pulse" alt="logo agency thirteen">
				</span>
					<h1 class="bottom"><?php _e('Oooooooh NO !!!', 'nightmare'); ?></h1>
				</div>
				<p><?php _e('Ne soyez pas fou, mais l\'URL nest pas ici. Pour aggraver les choses, nous n\'avons mÃªme pas une page 404 drÃ´le pour en profiter', 'nightmare'); ?></p>
				<ul> 
					<li><?php _e('Check your spelling', 'nightmare'); ?></li>
					<li><?php printf(__('Return to the <a href="%s">home page</a>', 'nightmare'), home_url()); ?></li>
					<li><?php _e('Click the <a href="javascript:history.back()">Back</a> button', 'nightmare'); ?></li>
				</ul>
			</div>
</article>
<hr />
</div>

			<!-- Sectioncontact -->
<section id="contact">

	<div class="row">

		<div id="planificateur" class="small-12 large-4 columns">
			<div class="icon-wrap icon-hover">
				<a href="#" class="hi-icon icon-magic" title="planificateur projet">planificateur</a>
				<h3>projet</h3>
				<p>
					Vous souhaitez travailler avec nous? Lancer de notre planificateur pour commencer.
				</p>
				<a href="/planificateur/" class="ng-button gray">lancer</a>

			</div>
		</div>

		<div id="connection" class="small-12 large-4 columns">
			<div class="icon-wrap icon-hover">
				<a href="#" class="hi-icon icon-comment">connection</a>
				<h3>connection</h3>
				<span>+3 (0)607 555 310</span>
				<span>hello [at] agencythirteen.net</span>
				<span>Suivez nous sur Twitter</span>

			</div>
		</div>

		<div id="newsletter" class="small-12 large-4 columns">
			<div class="icon-wrap icon-hover">
				<a href="#" class="hi-icon icon-bookmark">newsletter</a>
				<h3>newsletter</h3>
				<p>
					Entrez votre adresse email ci-dessous pour recevoir les mises Ã  jour occasionnelles.
				</p>
				<a href="#" data-reveal-id="news-form" class="ng-button dimensional">votre email</a>

			</div>

		</div>

		<div id="news-form" class="reveal-modal tiny">
			
			<h3 class="lead"><span class="hi-icon icon-bookmark"></span> Newsletter</h3>
			<?php gravity_form(1) ?>
			<a class="close-reveal-modal"> <i class="fa fa-times-circle"></i>
			</a>
		</div>
	</div>
</section>
<!-- End Section contact -->
		

	</div>

		
<?php get_footer(); ?>