<?php

get_header(); ?>

<div class="row">


	<?php /* Start loop */ ?>	
	<?php while (have_posts()) : the_post(); ?>	
	<article <?php post_class() ?>
		id="post-
		<?php the_ID(); ?>	
		">
		<!-- Section Projet -->
		<div id="single-projets" class="entry-content">
			<div id="picture" class="large-8 small-12 column">
			<?php if (has_post_thumbnail( $post->ID ) ): ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' ); ?>
			<a href="<?php echo $image[0]; ?>" rel="lightbox"><?php the_post_thumbnail('night-full-portfolio'); ?></a> 

			<?php endif; ?>
			</div>
			
			<!-- Section Sidebar -->
			<div id="sidebar" class="large-4 small-12 column">
				<header>
					<div class="nav-projets">

					<span><?php previous_post_link('%link', '<i class="fa fa-caret-square-o-left"></i>'); ?></span>	
					<span><?php next_post_link('%link', '<i class="fa fa-caret-square-o-right"></i>'); ?></span>	
				</div>
				</header>
				<div class="category">
					<h2><?php the_title( ); ?></h2>
				</div>
				<div class="category">
					<i class="fa fa-tags"></i> <?php the_terms( $post->ID, 'projet-cat' ); ?>
				</div>
				<p class="description">
					<?php the_content( ); ?>
				</p>

				<div class="infos">
					<?php
						global $wp_query;
						$postid = $wp_query->post->ID;
						echo '<p><span>Clients :</span>' . get_post_meta($postid, 'Clients', true) .'</p>' ;
						echo '<p><span>Compétences :</span>' . get_post_meta($postid, 'Skill', true) .'</p>' ;
						echo '<a href="' . get_post_meta($postid, 'Site', true) .'" class="ng-button small gray ">Voir le projet <i class="fa fa-angle-right"></i></a>' ;
						wp_reset_query();
						?>
  
					
				</div>

				<div class="share">
					<?php echo do_shortcode( '[easy-share buttons="twitter,facebook,google,linkedin,buffer,love" counters=1 hide_names="yes" counter_pos="inside" native="no" ]') ?>
				</div>
				
		<hr>	
			
			</div>
			<!-- End Section Sidebar -->


		</div>
		<!-- End Section Projet -->
<?php endwhile; wp_reset_query();  // End the loop ?>
		<!-- Section Projet Relatif -->		
		<div id="relation-post">
			<div id="content-relation">
				<div class="content-side">
					<h5>Projets relatifs</h5>
					<span class="tagline">Encore plus de très grands projets</span>
				</div>

			<ul id="grid" class="large-block-grid-4 small-12 colum">

				<?php

     //WordPress loop for custom post type

     $my_query = new WP_Query( 'post_type=projets&categories=websites&posts_per_page=4' );
     while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
				<li>
					<?php
			          $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'night-thumb-portfolio' );
			          $url = $thumb['0']; 
			         ?>
					<a href="<?php the_permalink(); ?>">

						<div class="thumb">
							<img class="lazy" data-original="<?php echo $url; ?>"></div>

						<h4>
							<?php the_title(); ?></h4>

						<div class="category">
							<?php the_terms( $post->ID, 'projet-cat' ); ?></div>

					</a>
				</li>

				<?php endwhile;  wp_reset_query(); ?></ul>

			</div>
				
		</div>
		<!-- ENd Section Projet Relatif -->		


	</article>
	

	</div>
</div>
<?php get_footer(); ?>