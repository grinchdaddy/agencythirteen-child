<?php

require_once 'lib/enqueue-style.php';
require_once 'lib/widget.php';
require_once 'lib/theme-support.php';
require_once 'lib/entry-meta.php';
require_once 'inc/shop.php';
require_once 'inc/custom-post/custom-post.php';

//Function EDD
require_once 'inc/include/edd/download-count.php';


require_once 'inc/custom-post/projets.php';
// require_once 'inc/include/prettyphoto.php';

add_image_size( 'thumb-blog', 350, 350, true );
add_image_size( 'night-thumb-portfolio', 440, 310, array( 'center', 'top' ) );
add_image_size( 'night-full-portfolio', 770, 552, true );
add_image_size( 'night-blog-full', 1600, 550, true );
add_image_size( 'night-full', 730, 350, array( 'center', 'top' ) );

if ( ! function_exists( 'home_scripts' ) ) {
  function home_scripts() {
    // if( is_page_template('page-homepage.php')) {
    if ( is_front_page() ) {

      wp_enqueue_script( 'nightmare-script', get_stylesheet_directory_uri() . '/js/script.js', array(), '1.0.0', true );
      wp_enqueue_script( 'nightmare-FlowType', get_stylesheet_directory_uri() . '/js/vendor/flowtype.js', array(), '2.1.2', true );
      wp_enqueue_script( 'scrollreveal',  get_stylesheet_directory_uri() . '/bower_components/scrollReveal.js/dist/scrollReveal.min.js', array( 'jquery' ), '', true );

      wp_enqueue_script( 'nightmare-parallax', get_stylesheet_directory_uri() . '/js/vendor/jquery.parallax-1.1.3.js', array(), '1.1.3', true );

      wp_enqueue_script( 'nightmare-appear', get_stylesheet_directory_uri() . '/js/vendor/bower_components/appear/jquery.appear.js', array(), '', true );


      wp_enqueue_script( 'nightmare-home', get_stylesheet_directory_uri() . '/js/vendor/home.js', array(), '1.0.0', true );

      wp_enqueue_script( 'nightmare-script' );
      wp_enqueue_script( 'nightmare-scrollreveal' );


    }}}


add_action('wp_enqueue_script', 'home_scripts' );



// function portfolio() {
//   if ( !is_page_template ( 'archive-projets.php' ) ||  !is_singular( 'projets' )   ) {

//     wp_register_script( 'night-lazy', get_stylesheet_directory_uri() . '/js/vendor/jquery.lazyload.js', array(), '1.6.3', true );
//     wp_register_script( 'night-portfolio', get_stylesheet_directory_uri() . '/js/vendor/portfolio.js', array(), '2.1.2', true );
//     wp_register_script( 'night-mixitup', get_stylesheet_directory_uri() . '/js/vendor/jquery.mixitup.min.js', array(), '0.6.2', true );


//     wp_enqueue_script( 'night-mixitup' );
//     wp_enqueue_script( 'night-lazy' );
//     wp_enqueue_script( 'night-portfolio' );


//   }
// }
// add_action( 'wp_print_scripts', 'portfolio' ); // now just run the function


/*==========  Excerpt  ==========*/

function nightmare_excerpt_length( $length ) {
  return 10;
}
add_filter( 'excerpt_length', 'nightmare_excerpt_length', 999 );

// function new_excerpt_more($more) {
//        global $post;
//   return '...<br /> <a class="button tiny moretag" href="'. get_permalink($post->ID) . '"> Lire l\'article complet ...</a>';
// }
// add_filter('excerpt_more', 'new_excerpt_more');

function excerpt($limit) {
      $excerpt = explode(' ', get_the_excerpt(), $limit);
      if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';
      } else {
        $excerpt = implode(" ",$excerpt);
      } 
      $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
      return $excerpt;
    }

    function content($limit) {
      $content = explode(' ', get_the_content(), $limit);
      if (count($content)>=$limit) {
        array_pop($content);
        $content = implode(" ",$content).'...';
      } else {
        $content = implode(" ",$content);
      } 
      $content = preg_replace('/\[.+\]/','', $content);
      $content = apply_filters('the_content', $content); 
      $content = str_replace(']]>', ']]&gt;', $content);
      return $content;
    }

// Redirection login page
function redirect_login_page(){

 // Store for checking if this page equals wp-login.php
 $page_viewed = basename($_SERVER['REQUEST_URI']);

 // Where we want them to go
 $login_page  = get_bloginfo('url' ). '/login';

 // Two things happen here, we make sure we are on the login page
 // and we also make sure that the request isn't coming from a form
 // this ensures that our scripts & users can still log in and out.
 if( $page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {

  // And away they go...
  wp_redirect($login_page);
  exit();

 }
}

add_action('init','redirect_login_page');

function ngf_login_redirect($redirect_to, $request, $user)
{
    return (is_array($user->roles) && in_array('administrator', $user->roles)) ? admin_url() : site_url(). '/account';
} 
add_filter('login_redirect', 'ngf_login_redirect', 10, 3);
?>
