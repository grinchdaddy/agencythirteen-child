<?php
/*
Template Name: partenaires
 */
get_header(); ?>

	<div class="row">
	<div class="small-12 large-8 columns" id="content" role="main">
	<h1><?php the_title( ); ?></h1>
	<?php
	$args = array(
		'post_type' =>  'partenaire',
		'posts_per_page'  => -1,
	 );
	$query_name = new WP_Query( $args );
	?>
	<ul class="partenaires large-12 columns">
	<?php if ( $query_name->have_posts() ) : ?>
		<?php while ( $query_name->have_posts() ) : $query_name->the_post(); ?>
	
		<?php
			
				$key = '_link';
				$wdn_meta = get_post_meta($post->ID, $key, true);
			
				// if the field is set within wordpress, continue
				if (get_post_meta($post->ID, $key, true))  ?>
			
				  	<li class="large-12 columns">
					<a class="large-5 columns" href="<?php echo $wdn_meta  ?>"><?php the_post_thumbnail( ); ?></a> 
					<div class="large-7 columns"> <?php the_content( ); ?></div>
					</li>
					<li class="separation"></li>

	
	<?php endwhile;endif; wp_reset_query(); ?>
	</ul>
	<div class="action-partenaire">
	<p>Vous désirez devenir un nos partenaires ?</p>
	<a href="/contact" target="_blank" class="button radius tiny">Devenir notre partenaire</a>
</div>
<!-- End Devenir partenaire -->
	</div>

	<div class="side small-12 large-4 columns">
	 <?php // Dynamic Sidebar
	if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'widget-top' ) ) : ?>

	
	<?php endif; // End Dynamic Sidebar shop ?>
	 <?php // Dynamic Sidebar
	if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'blog' ) ) : ?>

	
	<?php endif; // End Dynamic Sidebar shop ?>
		</div>
		</div>
<?php get_footer(); ?>