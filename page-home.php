<?php
/*
Template Name: Homepage
*/
get_header('home');
 ?>
<div id="slider" class="hide-for-touch">
	<div id="tagline" class="midway-horizontal midway-vertical">
		<div class="row">
			<div class="logosvg"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo/a13_logo_svg.svg" alt="Agencythirteen creation site internet"></div>
		</div>
		<div class="row">
		<h1><strong>Agence web </strong>&amp; <strong>création de site internet </strong>Evreux</h1>
			<div class="tagline">Tous les jours nous créons des choses fantastiques</div>
		</div>

		<div class="row">
			<div class="tagline">et nous essayons  de rendre ce monde plus beaux</div>
		</div>

		<div id="action">

			<a href="/projets" class="ng-button turquoise">
				Voir nos travaux <i class="fa fa-angle-right"></i>
			</a>

			<a href="/agence-web-haute-normandie" class="ng-button river">
				Plus sur nous ! <i class="fa fa-angle-right"></i>
			</a>

		</div>

		<div class="share hidden-for-small-only">
		<?php echo do_shortcode( '[essb-fans width="100" style="colored" cols="4"] ') ?>
	</div>
</div>
	<span class="arrow">
		<i class="top_link fa fa-chevron-circle-down floating"></i>
	</span>
</div>


<!-- Top Bar -->
<section id="top-bar">
	<header>
		<div class="row">

	
			<ul id="contact-top" class="large-6 columns">
				<li><i class="fa fa-phone"></i> +33 (0) 607555310</li>
				<li><i class="fa fa-envelope"></i> hello@agencythirteen.net</li>
				<li><i class="fa fa-briefcase"></i> <a href="/contact/">travailler  avec nous</a></li>
				
			</ul>

			<!-- Social Top -->

			<ul id="social-top" class="right">
				<li><a href="http://twitter.com/wecoastdesign"><i class="fa fa-twitter"></i></a></li>
				<li><a href="http://fr.linkedin.com/in/wecoastdesign"><i class="fa fa-linkedin"></i></a></li>
				<li><a href="https://plus.google.com/117880109778436499398/"><i class="fa fa-google-plus"></i></a></li>
				<li><a href="https://www.facebook.com/networkwecoastdesign"><i class="fa fa-facebook"></i></a></li>
				<li><a href="http://instagram.com/wecoastdesign/"><i class="fa fa-instagram"></i></a></li>
			</ul>

			<!-- End Social Top -->

		</div>
	</header>
</section>
<!-- End Top Bar -->

<div class="contain-to-grid">
	<!-- Starting the Top-Bar -->
	<nav class="top-bar" data-topbar>
	    <ul class="title-area">
	        <li class="name">
	        	<h1>
	        		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">

	        			<?php global $options;
	        				$logo_image = $options ['imglogo'] ['url'];
	        				
	        				if (! empty ($logo_image)){
	        					echo '<img src="'. $logo_image .'" class="logo-img" alt="">';
	        				} else {
	        					$logo_text = bloginfo('name');
	        					echo $logo_text;
	        				}
	        			 ?>
	        		</a>
	        	</h1>
	        </li>
			<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
			<li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
	    </ul>
	    <section class="top-bar-section">
			<?php if (function_exists('nightmare_top_bar_r')) { echo nightmare_top_bar_r( ); } ?>
	    </section>
	</nav>
	<!-- End of Top-Bar -->
</div>

<!-- Start the main container -->



<!-- Section Services -->

<section id="services">

	<div id="services-container">
	<div class="intro large-12 column">	
	<h1><strong>agence web</strong> à évreux haute normandie</h1>
	<h3>
		<span class="bold">Nous concevons</span>
		site internet, applications mobiles, Identité visuelle et plus
	</h3>
	<p>
		Nous sommes une <a href="/agence-web-haute-normandie/"><strong>agence web</strong></a> de <a href="/planificateur"><strong>création de site internet</strong></a> et de solution web, dédiée aux entreprises motivantes. Situé à <a href="https://maps.google.fr/maps/ms?msa=0&msid=203673160681163682793.000435fcbb2b2e8b728cf&dg=feature"><strong>Evreux</strong></a> à 1 heure de <strong>paris</strong>. Nous sommes dirigés par notre désir de produire un excellent travail et notre amour pour la vie, les gens, et les possibilités.
		Notre objectif est clair: produire des solutions numériques brillantes qui ont fière allure et surtout vous aidez à développer votre entreprise.
	</p>

	<h3 class="text-center">Pourquoi choisir notre agence web ?</h3>

	<h4>Pour être libre lors de la création de site internet!</h4>

	<p class="text-center">
		<strong>L’agence web </strong>Agencythirteen utilise un framework <a href="http://fr.wordpress.org/" title="Wordpress">wordpress</a>
		web basés sur <a href="http://foundation.zurb.com/" title="Foundation 5">Foundation 5</a>.<br />

		Son nom est	<a href="http://nightmareframework.net/" title="Wordpress Framework">Nightmare Framework</a>

		La <strong>création de site internet</strong>	n’a jamais été aussi simple.
	</p>

	<figure>
		<img src="http://gfx.wecoastdesign.com/nightmare/logo/logo-website.png" title="Nightmare Framework"/>	
	</figure>

	<p class="text-center">
		<a href="http://nightmareframework.net/" title="Nightmare Framework">Nightmare Framework</a> est un logiciel régi par un contrat d&#39;utilisation entre l&#39;utilisateur.<br />

		Ce contrat d&#39;utilisation donne à l&#39;utilisateur la possibilité bien sûr d&#39;utiliser le logiciel, mais également d&#39;accéder au code source et de le modifier à volonté.
	</p>


	</div>
<hr>
	<div class="services">

		<div id="dev" class="large-4 columns">

			<img src="<?php echo get_stylesheet_directory_uri() ?>/img/icons/develop.png" alt="developpement site web">
			<h4>Developpement Web</h4>
			<p>
				Nous utilisons les dernières technologies et techniques pour faire fonctionner le web pour votre entreprise et vos clients.
			</p>
		</div>

		<div id="ui-ux" class="large-4 columns">

			<img src="<?php echo get_stylesheet_directory_uri() ?>/img/icons/ui-ux.png" alt="creation site internet ">
			<h4>Design  UI / UX</h4>
			<p>
			Nous travaillons dur pour que vos utilisateurs puissent profiter de chaque seconde qu'ils passent avec votre produit.
			</p>
		</div>

		<div id="mobile" class="large-4 columns">

			<img src="<?php echo get_stylesheet_directory_uri() ?>/img/icons/mobile.png" alt="agence web pour application mobile">
			<h4>Mobile Plateforme</h4>
			<p>
				 Nous avons perfectionné notre formule pour produire des designs d'applications mobiles qui dépassent toutes les attentes.
			</p>
		</div>

	</div>
	</div>
</section>

<!-- End Section Services -->

<!-- Divider Parallax -->
<section id="divider-container">
	<div id="divider">
		<div class="row">
			<div class="tagline">nous concevons et développons des produits numériques</div>
		</div>

		<div class="row">
			<div class="tagline">pour web et mobile que les gens vont adorer utiliser</div>
		</div>

		<a href="/projets" class="ng-button rounded river">Venez voir ce que nous voulons dire</a>

	</div>
</section>
<!-- End Divider Parallax -->

<!-- Sectioncontact -->
<section id="contact">

	<div class="row">

		<div id="planificateur" class="large-4 columns">
			<div class="icon-wrap icon-hover">
				<a href="#" class="hi-icon icon-magic" title="planificateur projet">planificateur</a>
				<h3>projet</h3>
				<p>
					Vous souhaitez travailler avec nous? Lancer de notre planificateur pour commencer.
				</p>
				<a href="/planificateur/" class="ng-button gray">lancer</a>

			</div>
		</div>

		<div id="connection" class="large-4 columns">
			<div class="icon-wrap icon-hover">
				<a href="#" class="hi-icon icon-comment">connection</a>
				<h3>connection</h3>
				<span class="infos">+3 (0)607 555 310</span>
				<span class="infos">hello [at] agencythirteen.net</span>
				<span class="infos">Suivez nous sur Twitter</span>
				<?php echo do_shortcode( '[easy-social-like twitter_follow="true" twitter_follow_user="agencythirteen" twitter_follow_skinned_text="Suivez-nous" twitter_follow_skinned_width="150" counters="true" skinned="true" skin="metro" 
				facebook="true" facebook_width="100" facebook_url="https://www.facebook.com/agencythirteen"]') ?>
			</div>
		</div>

		<div id="newsletter" class="large-4 columns">
			<div class="icon-wrap icon-hover">
				<a href="#" class="hi-icon icon-bookmark">newsletter</a>
				<h3>newsletter</h3>
				<p>
					Entrez votre adresse email ci-dessous pour recevoir les mises à jour occasionnelles.
				</p>
				<a href="#" data-reveal-id="news-form" class="ng-button dimensional">votre email</a>

			</div>

		</div>

		<div id="news-form" class="reveal-modal tiny">
			
			
			<?php gravity_form(1) ?>
			<a class="close-reveal-modal"> <i class="fa fa-times-circle"></i>
			</a>
		</div>
	</div>
</section>
<!-- End Section contact -->


</div>
		
<?php get_footer(); ?>