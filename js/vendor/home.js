/*==========  Adaptation de l'ecran  ==========*/
function adpaterALaTailleDeLaFenetre(){
  var largeur = document.documentElement.clientWidth,
  hauteur = document.documentElement.clientHeight;
   
  var source = document.getElementById('slider'); // récupère l'id source
  source.style.height = hauteur+'px'; // applique la hauteur de la page
  source.style.width = largeur+'px'; // la largeur
}
 
// Une fonction de compatibilité pour gérer les évènements
function addEvent(element, type, listener){
  if(element.addEventListener){
    element.addEventListener(type, listener, false);
  }else if(element.attachEvent){
    element.attachEvent("on"+type, listener);
  }
}
 
// On exécute la fonction une première fois au chargement de la page
addEvent(window, "load", adpaterALaTailleDeLaFenetre);
// Puis à chaque fois que la fenêtre est redimensionnée
addEvent(window, "resize", adpaterALaTailleDeLaFenetre);

/*==========  Fading Elements  ==========*/
jQuery(window).scroll(function(){
 var posScroll = jQuery(document).scrollTop();
  if(posScroll >=400)
   {jQuery('.top_link').fadeOut("slow");}
 else
  {jQuery('.top_link').fadeIn("slow");}

});

/*==========  SVG Support  ==========*/
if(!Modernizr.svg) {
    jQuery('img[src*="svg"]').attr('src', function() {
        return jQuery(this).attr('src').replace('.svg', '.png');
    });
}
