(function($){

    $(document).ready(function(){
      // Selectize
      $('select').selectize();
     // Icheck
        $('input').iCheck({
    checkboxClass: 'icheckbox_flat-red',
    radioClass: 'iradio_flat-red'
  });
    });

})(jQuery);