(function(e, t, n, r) {
    "use strict";
    var i = {
        version: "0.0.1",
        settings: {
            donut: !1,
            donut_inner_ratio: .4,
            percent_offset: 35,
            stroke_color: "#333",
            stroke_width: 0,
            show_percent: !0,
            animation_speed: 500,
            always_show_percent: !1,
            animation_type: "elastic"
        },
        init: function(t, r) {
            var i = this;
            this.scope = t || n.body;
            var s = e("[data-pie-id]", this.scope);
            e.extend(!0, this.settings, r);
            s.length > 0 ? s.each(function() {
                return i.build(e(this), r);
            }) : this.build(e(this.scope), r);
            this.events();
        },
        events: function() {
            var n = this;
            e(t).off(".pizza").on("resize.pizza", n.throttle(function() {
                n.init();
            }, 100));
            e(this.scope).off(".pizza").on("mouseenter.pizza mouseleave.pizza touchstart.pizza", "[data-pie-id] li", function(t) {
                var n = e(this).parent(), r = Snap(e("#" + n.data("pie-id") + ' path[data-id="s' + e(this).index() + '"]')[0]), i = Snap(e(r.node).parent().find('text[data-id="' + r.node.getAttribute("data-id") + '"]')[0]), s = e(this).parent().data("settings");
                /start/i.test(t.type) && e(r.node).siblings("path").each(function() {
                    if (this.nodeName) {
                        r.animate({
                            transform: "s1 1 " + r.node.getAttribute("data-cx") + " " + r.node.getAttribute("data-cy")
                        }, s.animation_speed, mina[s.animation_type]);
                        Snap(e(this).next()[0]).animate({
                            opacity: 0
                        }, s.animation_speed);
                    }
                });
                if (/enter|start/i.test(t.type)) {
                    r.animate({
                        transform: "s1.05 1.05 " + r.node.getAttribute("data-cx") + " " + r.node.getAttribute("data-cy")
                    }, s.animation_speed, mina[s.animation_type]);
                    s.show_percent && i.animate({
                        opacity: 1
                    }, s.animation_speed);
                } else {
                    r.animate({
                        transform: "s1 1 " + r.node.getAttribute("data-cx") + " " + r.node.getAttribute("data-cy")
                    }, s.animation_speed, mina[s.animation_type]);
                    i.animate({
                        opacity: 0
                    }, s.animation_speed);
                }
            });
        },
        build: function(t, n) {
            var r = this, i = t, s;
            i.data("settings", e.extend({}, r.settings, n, i.data("options")));
            r.data(i, n || {});
            return r.update_DOM(r.pie(i));
        },
        data: function(t, n) {
            var r = [], i = 0;
            e("li", t).each(function() {
                var t = e(this);
                n.data ? r.push({
                    value: n.data[t.index()],
                    color: t.css("color"),
                    segment: t
                }) : r.push({
                    value: t.data("value"),
                    color: t.css("color"),
                    segment: t
                });
            });
            return t.data("graph-data", r);
        },
        update_DOM: function(t) {
            var n = t[0], r = t[1];
            return e(this.identifier(n)).html(r);
        },
        pie: function(t) {
            var n = t.data("settings"), r = this.svg(t, n), i = t.data("graph-data"), s = 0, o = [], u = 0, a = e(this.identifier(t)).width() - 4;
            for (var f = 0; f < i.length; f++) s += i[f].value;
            for (var f = 0; f < i.length; f++) o[f] = i[f].value / s * Math.PI * 2;
            for (var f = 0; f < i.length; f++) {
                var l = u + o[f], c = a / 2, h = a / 2, p = a / 2 * .85;
                if (!n.donut) {
                    var d = c + p * Math.sin(u), v = h - p * Math.cos(u), m = c + p * Math.sin(l), g = h - p * Math.cos(l), y = 0;
                    l - u > Math.PI && (y = 1);
                    var b = "M" + c + "," + h + " L" + d + "," + v + " A" + p + "," + p + " 0 " + y + " 1 " + m + "," + g + " Z";
                }
                var w = e('path[data-id="s' + f + '"]', r.node);
                if (w.length > 0) var E = Snap(w[0]); else var E = r.path();
                var S = i[f].value / s * 100, x = e('text[data-id="s' + f + '"]', r.node);
                if (x.length > 0) {
                    var T = Snap(x[0]);
                    T.attr({
                        x: c + (p + n.percent_offset) * Math.sin(u + o[f] / 2),
                        y: h - (p + n.percent_offset) * Math.cos(u + o[f] / 2)
                    });
                } else var T = E.paper.text(c + (p + n.percent_offset) * Math.sin(u + o[f] / 2), h - (p + n.percent_offset) * Math.cos(u + o[f] / 2), Math.ceil(S) + "%");
                var N = T.getBBox().width / 2;
                n.always_show_percent ? T.attr({
                    x: T.attr("x") - N,
                    opacity: 1
                }) : T.attr({
                    x: T.attr("x") - N,
                    opacity: 0
                });
                T.node.setAttribute("data-id", "s" + f);
                E.node.setAttribute("data-cx", c);
                E.node.setAttribute("data-cy", h);
                n.donut ? this.annular_sector(E.node, {
                    centerX: c,
                    centerY: h,
                    startDegrees: u,
                    endDegrees: l,
                    innerRadius: p * n.donut_inner_ratio,
                    outerRadius: p
                }) : E.attr({
                    d: b
                });
                E.attr({
                    fill: i[f].color,
                    stroke: n.stroke_color,
                    strokeWidth: n.stroke_width
                });
                E.node.setAttribute("data-id", "s" + f);
                this.animate(E, c, h, n);
                u = l;
            }
            return [ t, r.node ];
        },
        animate: function(t, n, r, i) {
            var s = this;
            t.hover(function(t) {
                var s = Snap(t.target), o = Snap(e(s.node).parent().find('text[data-id="' + s.node.getAttribute("data-id") + '"]')[0]);
                s.animate({
                    transform: "s1.05 1.05 " + n + " " + r
                }, i.animation_speed, mina[i.animation_type]);
                o.touchend(function() {
                    s.animate({
                        transform: "s1.05 1.05 " + n + " " + r
                    }, i.animation_speed, mina[i.animation_type]);
                });
                if (i.show_percent) {
                    o.animate({
                        opacity: 1
                    }, i.animation_speed);
                    o.touchend(function() {
                        o.animate({
                            opacity: 1
                        }, i.animation_speed);
                    });
                }
            }, function(t) {
                var s = Snap(t.target), o = Snap(e(s.node).parent().find('text[data-id="' + s.node.getAttribute("data-id") + '"]')[0]);
                s.animate({
                    transform: "s1 1 " + n + " " + r
                }, i.animation_speed, mina[i.animation_type]);
                o.animate({
                    opacity: 0
                }, i.animation_speed);
            });
        },
        svg: function(t, n) {
            var r = e(this.identifier(t)), i = e("svg", r), s = r.width(), o = s;
            i.length > 0 ? i = Snap(i[0]) : i = Snap(s, o);
            i.node.setAttribute("width", s + n.percent_offset);
            i.node.setAttribute("height", o + n.percent_offset);
            i.node.setAttribute("viewBox", "-" + n.percent_offset + " -" + n.percent_offset + " " + (s + n.percent_offset * 1.5) + " " + (o + n.percent_offset * 1.5));
            return i;
        },
        annular_sector: function(e, t) {
            function a(e) {
                var t = {
                    cx: e.centerX || 0,
                    cy: e.centerY || 0,
                    startRadians: e.startDegrees || 0,
                    closeRadians: e.endDegrees || 0
                }, n = e.thickness !== r ? e.thickness : 100;
                e.innerRadius !== r ? t.r1 = e.innerRadius : e.outerRadius !== r ? t.r1 = e.outerRadius - n : t.r1 = 200 - n;
                e.outerRadius !== r ? t.r2 = e.outerRadius : t.r2 = t.r1 + n;
                t.r1 < 0 && (t.r1 = 0);
                t.r2 < 0 && (t.r2 = 0);
                return t;
            }
            var n = a(t), i = [ [ n.cx + n.r2 * Math.sin(n.startRadians), n.cy - n.r2 * Math.cos(n.startRadians) ], [ n.cx + n.r2 * Math.sin(n.closeRadians), n.cy - n.r2 * Math.cos(n.closeRadians) ], [ n.cx + n.r1 * Math.sin(n.closeRadians), n.cy - n.r1 * Math.cos(n.closeRadians) ], [ n.cx + n.r1 * Math.sin(n.startRadians), n.cy - n.r1 * Math.cos(n.startRadians) ] ], s = n.closeRadians - n.startRadians, o = s % (Math.PI * 2) > Math.PI ? 1 : 0, u = [];
            u.push("M" + i[0].join());
            u.push("A" + [ n.r2, n.r2, 0, o, 1, i[1] ].join());
            u.push("L" + i[2].join());
            u.push("A" + [ n.r1, n.r1, 0, o, 0, i[3] ].join());
            u.push("z");
            e.setAttribute("d", u.join(" "));
        },
        identifier: function(e) {
            return "#" + e.data("pie-id");
        },
        throttle: function(e, t) {
            var n = null;
            return function() {
                var r = this, i = arguments;
                clearTimeout(n);
                n = setTimeout(function() {
                    e.apply(r, i);
                }, t);
            };
        }
    };
    t.Pizza = i;
})($, this, this.document);