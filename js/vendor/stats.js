/*==========  Statistiques  ==========*/

jQuery(function() {


    // jQuery(window).scroll(function() {
    //     jQuery('.view').each(function() {
    //         var imagePos = jQuery(this).offset().top;

    //         var topOfWindow = jQuery(window).scrollTop();
    //         if (imagePos < topOfWindow + 300) {
    //             jQuery(this).addClass("animated fadeIn");
    //         }
    //     });
    // });


    jQuery('.view').waypoint(function() {
        jQuery(this).addClass('animated fadeIn');
    }, {
        triggerOnce: true,
        offset: 'bottom-in-view'
    });

    jQuery('.year').waypoint(function() {
        jQuery(this).easyPieChart({
            animate: 4000,
            trackColor: '#e4e4e4',
            scaleColor: false,
            barColor: "#3498db",
            size: 120,
            onStep: function(from, to, percent) {
                jQuery(this.el).find('.annees').text(Math.round(percent));
            },
        });
    }, {
        triggerOnce: true,
        offset: 'bottom-in-view'
    });

    jQuery('.projets').waypoint(function() {
        jQuery(this).easyPieChart({
            animate: 4000,
            trackColor: '#e4e4e4',
            scaleColor: false,
            barColor: "#3498db",
            size: 120,
            onStep: function(from, to, percent) {
                jQuery(this.el).find('.percent').text(Math.round(percent));
            },
        });
    }, {
        triggerOnce: true,
        offset: 'bottom-in-view'
    });

    jQuery('.clients').waypoint(function() {
        jQuery(this).easyPieChart({
            animate: 4000,
            trackColor: '#e4e4e4',
            scaleColor: false,
            barColor: "#3498db",
            size: 120,
            onStep: function(from, to, percent) {
                jQuery(this.el).find('.percent').text(Math.round(percent));
            },
        });
    }, {
        triggerOnce: true,
        offset: 'bottom-in-view'
    });

    jQuery('.coffee').waypoint(function() {
        jQuery(this).easyPieChart({
            animate: 4000,
            trackColor: '#e4e4e4',
            scaleColor: false,
            barColor: "#f39c12",
            size: 120,
            onStep: function(from, to, percent) {
                jQuery(this.el).find('.tasses').text(Math.round(percent));
            },
        });
    }, {
        triggerOnce: true,
        offset: 'bottom-in-view'
    });

});
