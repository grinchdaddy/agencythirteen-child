/*Icheck*/jQuery(document).ready(function() {
    jQuery("input").each(function() {
        var e = jQuery(this), t = e.next(), n = t.text();
        t.remove();
        e.iCheck({
            checkboxClass: "icheckbox_line-red",
            radioClass: "iradio_line-red",
            hoverClass: "hover",
            insert: '<div class="icheck_line-icon"></div>' + n
        });
    });
});

jQuery("select").selectize();