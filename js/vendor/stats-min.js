/*==========  Statistiques  ==========*/
jQuery(function() {
    jQuery(".view").waypoint(function() {
        jQuery(this).addClass("animated fadeIn");
    }, {
        triggerOnce: !0,
        offset: "bottom-in-view"
    });
    jQuery(".year").waypoint(function() {
        jQuery(this).easyPieChart({
            animate: 4e3,
            trackColor: "#e4e4e4",
            scaleColor: !1,
            barColor: "#3498db",
            size: 120,
            onStep: function(e, t, n) {
                jQuery(this.el).find(".annees").text(Math.round(n));
            }
        });
    }, {
        triggerOnce: !0,
        offset: "bottom-in-view"
    });
    jQuery(".projets").waypoint(function() {
        jQuery(this).easyPieChart({
            animate: 4e3,
            trackColor: "#e4e4e4",
            scaleColor: !1,
            barColor: "#3498db",
            size: 120,
            onStep: function(e, t, n) {
                jQuery(this.el).find(".percent").text(Math.round(n));
            }
        });
    }, {
        triggerOnce: !0,
        offset: "bottom-in-view"
    });
    jQuery(".clients").waypoint(function() {
        jQuery(this).easyPieChart({
            animate: 4e3,
            trackColor: "#e4e4e4",
            scaleColor: !1,
            barColor: "#3498db",
            size: 120,
            onStep: function(e, t, n) {
                jQuery(this.el).find(".percent").text(Math.round(n));
            }
        });
    }, {
        triggerOnce: !0,
        offset: "bottom-in-view"
    });
    jQuery(".coffee").waypoint(function() {
        jQuery(this).easyPieChart({
            animate: 4e3,
            trackColor: "#e4e4e4",
            scaleColor: !1,
            barColor: "#f39c12",
            size: 120,
            onStep: function(e, t, n) {
                jQuery(this.el).find(".tasses").text(Math.round(n));
            }
        });
    }, {
        triggerOnce: !0,
        offset: "bottom-in-view"
    });
});