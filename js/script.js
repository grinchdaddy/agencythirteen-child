
//Backstrech
  //jQuery("#slider").backstretch();
 //Preloader
 // jQuery(document).ready(function() {
 //    jQuery('body').jpreLoader();
 //  });

 /*Parallax*/
jQuery(document).ready(function(){
 jQuery('#slider').parallax("50%", 0.1);
 jQuery('#divider-container').parallax("50%", 0.1);

});



 jQuery(document).ready(function() {

 window.scrollReveal = new scrollReveal();
});
/*==========  FLowtype  ==========*/


 (function($){
 
     $(document).ready(function(){
$('h1').flowtype({
fontRatio : 12,
lineRatio : 1.15,
minFont : 25,
maxFont : 50,

});
$('#slider h1').flowtype({
fontRatio : 12,
lineRatio : 1.15,
minFont : 25,
maxFont : 30,

});

$('h2').flowtype({
    fontRatio : 12,
    lineRatio : 1.15,
    minFont : 18,
    maxFont : 30,
});

$('h3').flowtype({
    fontRatio : 5,
    lineRatio : 1.15,
    minFont : 15,
    maxFont : 25,
});

$('.intro h3').flowtype({
    fontRatio : 5,
    lineRatio : 1.15,
    minFont : 20,
    maxFont : 25,
});

$('input[type="submit"]').flowtype({
    fontRatio : 5,
    lineRatio : 1.15,
    minFont : 12,
    maxFont : 20,
});

$('.title-full').flowtype({
    fontRatio : 5,
    lineRatio : 1.15,
    minFont : 12,
    maxFont : 30,
});
     });

 
 })(jQuery);