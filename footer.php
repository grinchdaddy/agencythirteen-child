<!-- Section Footer Top -->
<section id="footer-top">
	<div class="small-10 small-centered large-12 column ">
		<p>copyright © 2009-2013 . Tous droits réservés <a href="http://agencythirteen.net" title="agency thirteen" target="_blank">[agency Thirteen]</a> <br />Agencty thirteen est un service de <a href="http://wecoastdesign.com">[wecoastdesign network]</a> <br />enregistré en france sous le n° 51111886100026</p>
		<p><a href="/mentions-legales">mentions légales</a></p>
	</div>
</section>
<!-- End Section Footer Top -->

<!-- Section Footer Bottom -->
<section  id="footer-bottom" role="contentinfo">
<div class="row">
	<div id="tagline-footer">
	<div id="copy-container">
		<a href="http://agencythirteen.net" class="icon-agency">agency thirteen</a>
		<span class="agence">UNE AGENCE DIGITALE INTERACTIVE</span>
		</div>
	</div>
</div>
</section>
<!-- Section End Footer Bottom -->

<!-- section Copyright -->
<section id="copyright">
<div class="row">
	<div class="framework">
		<p>Crafted with <a href="http://nightmare.agencythirteen.net" title="nightmare framework">Nightmare Framework</a></p>
	</div>

		<div id="nav-footer">
			<p>
				<a href="http://twitter.com/agencythirteen" target="_blank">@agencythirteen</a>
				<a href="mailto:contact@wecoastdesign.com">Nous Dire Bonjour →</a>
			</p>
		</div>
	</div>
</section>
<!-- End section Copyright -->

<?php wp_footer(); ?>

<script>
	(function($) {
		$(document).foundation();
	})(jQuery);
</script>

</html>