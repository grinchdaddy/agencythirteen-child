<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" <?php language_attributes(); ?> > <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" <?php language_attributes(); ?> > <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" <?php language_attributes(); ?> "> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?> > <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset'); ?>">

	<title><?php wp_title('|', true, 'right'); bloginfo('name'); ?></title>

	<!-- Mobile viewport optimized: j.mp/bplateviewport -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<!-- Favicon and Feed -->
	<link rel="shortcut icon" type="image/png" href="<?php global $options; echo $options ['favicon'] ['url'] ?> ">
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">

	<!--  iPhone Web App Home Screen Icon -->
	<link rel="apple-touch-icon" sizes="72x72" href="<?php global $options;  echo $options ['icon-iphone'] ['url'] ?>" />
	<link rel="apple-touch-icon" sizes="114x114" href="<?php global $options;  echo $options ['icon-retina'] ['url'] ?>" />
	<link rel="apple-touch-icon" href="<?php global $options;  echo $options ['icon-iphone'] ['url'] ?>" />

	<!-- Enable Startup Image for iOS Home Screen Web App -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<link rel="apple-touch-startup-image" href="<?php echo get_template_directory_uri(); ?>/mobile-load.png" />

	<!-- Startup Image iPad Landscape (748x1024) -->
	<link rel="apple-touch-startup-image" href="<?php global $options;  echo $options ['startup-landscape'] ['url'] ?>" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)" />
	<!-- Startup Image iPad Portrait (768x1004) -->
	<link rel="apple-touch-startup-image" href="<?php global $options; echo $options ['startup-portrait'] ['url'] ?>" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)" />
	<!-- Startup Image iPhone (320x460) -->
	<link rel="apple-touch-startup-image" href="<?php global $options;  echo $options ['mobile-load'] ['url'] ?>" media="screen and (max-device-width: 320px)" />

<?php wp_head(); ?>

</head>

<body <?php body_class('antialiased shop'); ?>>
	<div class="contain-to-grid">
	<!-- Starting the Top-Bar -->
	<nav class="top-bar" data-topbar>
	    <ul class="title-area">
	        <li class="name">
	        	<h1>
	        		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">

	        			<?php global $options;
	        				$logo_image = $options ['imglogo'] ['url'];
	        				
	        				if (! empty ($logo_image)){
	        					echo '<img src="'. $logo_image .'" class="logo-img" alt="">';
	        				} else {
	        					$logo_text = bloginfo('name');
	        					echo $logo_text;
	        				}
	        			 ?>
	        		</a>
	        	</h1>
	        </li>
			<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
			<li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
	    </ul>
	    <section class="top-bar-section">
			<?php if (function_exists('nightmare_top_bar_r')) { echo nightmare_top_bar_r( ); } ?>
	    </section>
	</nav>
	<!-- End of Top-Bar -->
</div>


<div id="header-shop">
		<?php navigat ?>
		<div class="cart">
			<a href="<?php echo edd_get_checkout_uri(); ?>">
			Cart (<span class="header-cart edd-cart-quantity"><?php echo edd_get_cart_quantity(); ?></span>)
			</a>
		</div>
		<div class="login">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>login">Login</a>
		</div>
	</div>
<h1 class="logo-store">
	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">

	<img src="<?php echo get_stylesheet_directory_uri() ?>/img/logo/logo-store.png" alt="logo du store">
	</a>
</h1>


